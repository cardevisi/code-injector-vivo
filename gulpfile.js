const gulp = require('gulp');
const concat = require('gulp-concat-util');
const browserSync = require('browser-sync').create();

gulp.task('scripts', function(){
    return gulp.src([
        './app/module/DebugTracking.js',
        './app/clients/promo.vivofibra.com.br/FormElements.js',
        './app/module/init.js'
    ])
    .pipe(concat('main.js'))
    .pipe(concat.header('(function(window, document, undefined){\n\'use strict\';\n'))
    .pipe(concat.footer('\n})(window, document, undefined);\n'))
    .pipe(gulp.dest('./dist/vivo-empresas/js/'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('server', ['scripts'], function(){
    browserSync.init({
        server: "./"
    });

    gulp.watch("./app/modules/*.js", ['scripts']);
    gulp.watch("./app/clients/promo.vivofibra.com.br/*.js", ['scripts']);
    gulp.watch("./index.html").on('change', browserSync.reload);
});

gulp.task('default', ['server']);