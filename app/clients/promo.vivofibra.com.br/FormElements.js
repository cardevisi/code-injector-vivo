function FormElements() {

    window.triggerUOLTM = window.triggerUOLTM || [];
    
    var TOTAL_TIME = 1000;
    
    function findModalChat () {
        var count = 0;
        function loop() {
            var modalChatIframe = document.querySelector('.modal.chat-iframe') || document.querySelector('div.lightbox.chat-iframe');
            if(modalChatIframe) {
                DebugTracking.showMessage('::: FOUND MODAL.CHAT-IFRAME OR LIGHTBOX.CHAT-IFRAME ::: and ::: gatilho-vivo-empresa-compre-chat :::');
                window.triggerUOLTM.push({'eventName': 'gatilho-vivo-empresa-compre-chat'});
            } else if(count < TOTAL_TIME) {
                DebugTracking.showMessage('::: FIND LOOP - MODAL.CHAT-IFRAME :::');
                setTimeout(loop, 1000);
                count++;
            }
        }
        loop();
    }

    function findModalMonteSeuCombo () {
        var count = 0;
        function loop() {
            var monteSeuCombo = document.querySelector('.modal.consulta-cep');
            if(monteSeuCombo) {
                DebugTracking.showMessage('::: FOUND MODAL.CHAT-IFRAME ::: and ::: gatilho-vivo-empresa-abertura-modal :::');
                window.triggerUOLTM.push({'eventName': 'gatilho-vivo-empresa-abertura-modal'});
            } else if(count < TOTAL_TIME) {
                DebugTracking.showMessage('::: FIND LOOP - MODAL.CONSULTA-CEP :::');
                setTimeout(loop, 1000);
                count++;
            }
        }
        loop();
    }

    function findModalComprePorTelefone () {
        var count = 0;
        function loop() {
            var monteSeuCombo = document.querySelector('.modal.compre-telefone');
            if(monteSeuCombo) {
                DebugTracking.showMessage('::: FOUND COMPRE POR TELEFONE ::: and ::: gatilho-vivo-empresa-compre-por-telefone :::');
                window.triggerUOLTM.push({'eventName': 'gatilho-vivo-empresa-compre-por-telefone'});
            } else if(count < TOTAL_TIME) {
                DebugTracking.showMessage('::: FIND LOOP - COMPRE POR TELEFONE :::');
                setTimeout(loop, 1000);
                count++;
            }
        }
        loop();
    }

    function findElement(selector, callback) {
        var count = 0;
        function loop() {
            var element = document.querySelector(selector);
            if(element) {
                callback(element);
            } else if(count < TOTAL_TIME) {
                DebugTracking.showMessage('::: FIND BY -' + selector + ' :::');
                setTimeout(loop, 1000);
                count++;
            }
        };
        loop();
    }

    function validateFormPreenchaDados () {
        var form = document.querySelector('#preencha-dados');
        var inputs = form.querySelectorAll('input');
        for (var i = 0; i < inputs.length; i++) {
            var currentInput = inputs[i];
            var attrClass = currentInput.getAttribute('class');
            if ((currentInput && currentInput.value == '') && (attrClass && attrClass.match(/error/) != null)) {
                return false;
            } else {
                continue;
            }
        }
        return true;
    }

    function validateFormDisponibilidade() {
        var form = document.querySelector('#valida_form');
        var inputs = form.querySelectorAll('input');
        for (var i = 0; i < inputs.length; i++) {
            var currentInput = inputs[i];
            //var attrClass = currentInput.getAttribute('class');
            //if (currentInput && (attrClass && attrClass.match(/error/) != null))
            if (currentInput && currentInput.value === '') {
                return false;
            } else {
                continue;
            }
        }
        return true;
    }

    this.init = function () {
        /************************************************************
        * Botãos da Barra de CTAS - Abaixo do Slider da página principal
        ************************************************************/
        var btnCompreChatBarraCtas = document.querySelector('section.barra-ctas > div > a:nth-child(3)');
        if(btnCompreChatBarraCtas) {
            DebugTracking.showMessage('::: CHAT BUTTON FOUNDED :::');
            btnCompreChatBarraCtas.addEventListener('click', function(){
                findModalChat();
            });
        }
        var btnCompreChat = document.querySelector('div.btn-assine-ja > ul > li:nth-child(3)');
        if(btnCompreChat) {
            DebugTracking.showMessage('::: CHAT BUTTON FOUNDED :::');
            btnCompreChat.addEventListener('click', function(){
                findModalChat();
            });
        }

        var btnCompreChatCarrinho = document.querySelector('.calls .comprechat');
        if(btnCompreChatCarrinho) {
            DebugTracking.showMessage('::: CHAT BUTTON CART FOUNDED :::');
            btnCompreChatCarrinho.addEventListener('click', function(){
                findModalChat();
            });
        }
        /******************************************/

        /************************************************************
        * Botãos monte seu combo
        ************************************************************/
        var btnMonteSeuCombo = document.querySelector('body > header > div.menu-principal > div > div.btn-assine-ja.hidden-xs > ul > li:nth-child(1)');
        if(btnMonteSeuCombo) {
            DebugTracking.showMessage('::: MONTE SEU COMBO ASSINE FOUNDED :::'); 
            btnMonteSeuCombo.addEventListener('click', function(){
                findModalMonteSeuCombo();
            });
        }

        var btnMonteSeuComboBarraCtas = document.querySelector('section.barra-ctas > div > a:nth-child(1)');
        if(btnMonteSeuComboBarraCtas) {
            DebugTracking.showMessage('::: MONTE SEU COMBO BARRA CTAS FOUNDED :::'); 
            btnMonteSeuComboBarraCtas.addEventListener('click', function(){
                findModalMonteSeuCombo();
            });
        }

        var btnsConsulteDisponibilidade = document.querySelectorAll('.btn-large.bg-purple.open-modal');
        for (var i = 0; i < btnsConsulteDisponibilidade.length; i++) {
            btnsConsulteDisponibilidade[i].addEventListener('click', function(){
                findModalMonteSeuCombo();
            });
        }

        var btnsAssineMenuCombo = document.querySelectorAll('.preco .btn-small');
        for (var j = 0; j < btnsAssineMenuCombo.length; j++) {
            btnsAssineMenuCombo[j].addEventListener('click', function(){
                findModalMonteSeuCombo();
            });
        }
        /******************************************/

        /************************************************************
        * Botãos compre por telefone
        ************************************************************/
        var btnCompreTelefoneBarraCtas = document.querySelector('section.barra-ctas > div > a:nth-child(2)');
        if(btnCompreTelefoneBarraCtas) {
            DebugTracking.showMessage('::: COMPRE PELO TELEFONE CTAS BUTTON FOUNDED :::');
            btnCompreTelefoneBarraCtas.addEventListener('click', function(){
                findModalComprePorTelefone();
            });
        }
        var btnCompreTelefoneAssine = document.querySelector('div.btn-assine-ja > ul > li:nth-child(2)');
        if(btnCompreTelefoneAssine) {
            DebugTracking.showMessage('::: COMPRE PELO TELEFONE ASSINE BUTTON FOUNDED :::');
            btnCompreTelefoneAssine.addEventListener('click', function(){
                findModalComprePorTelefone();
            });
        }
        /******************************************/

        /************************************************************
        * Botão continuar no carrinho
        ************************************************************/
        var btnContinarCart = document.querySelector('.button-orange.sign-in[title*=Continuar]')
        if(btnContinarCart) {
            DebugTracking.showMessage('::: CONTINUE BUTTON FOUNDED :::');
            btnContinarCart.addEventListener('click', function(){
                DebugTracking.showMessage('::: gatilho-vivo-empresa-carrinho-continuar :::');
                window.triggerUOLTM.push({'eventName': 'gatilho-vivo-empresa-carrinho-continuar'});
            });
        }

       
        /************************************************************
        * Botão submit Consulte disponibilidade
        ************************************************************/
        var submitButtonConsulteDisponibilidade = document.querySelector('input.btn-small.purple[value*=Consultar]');
        if(submitButtonConsulteDisponibilidade) {
            submitButtonConsulteDisponibilidade.addEventListener('click', function(){
                var validate = validateFormDisponibilidade();
                if(validate) {
                    DebugTracking.showMessage('::: gatilho-vivo-empresa-submit-consulte-disponibilidade :::');
                    window.triggerUOLTM.push({'eventName': 'gatilho-vivo-empresa-submit-consulte-disponibilidade'});
                }
            }, false);
        }

        /************************************************************
        * Botão submit Finalizar Compra
        ************************************************************/
        findElement('#preencha-dados > div.button > input', function(element) {
            if(element) {
                element.addEventListener('click', function(){
                    setTimeout(function(){
                        var validate = validateFormPreenchaDados();
                        if (validate) {
                            DebugTracking.showMessage('::: CHAMA GATILHO FINALIZAR COMPRA :::');
                        }
                    }, 0);
                }, false);
            }
        });
        
        DebugTracking.showMessage('::: INIT :::');
    }
}