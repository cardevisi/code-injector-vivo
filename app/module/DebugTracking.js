var DebugTracking = {
    setDebug : function (){
        localStorage.setItem('debug_tm', 'true');
    },
    getDebug : function (){
        return localStorage.getItem('debug_tm');
    },
    clearDebug : function (){
        localStorage.removeItem('debug_tm');
    },
    showMessage : function () {
        var message = [];
        for (var i = 0; i <= arguments.length; i++) {
            message.push(arguments[i]);
        }
        if (this.getDebug() === 'true') console.log(message.join(' '));
    }
};